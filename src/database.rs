use serde::{Serialize, Deserialize};
use std::{thread, fs};
use chrono::{Duration, Utc};
use reqwest::Url;

use crate::http::get_http_client;

pub type Id = u32;

#[derive(Debug, Serialize, Deserialize)]
pub struct Database {
    root_id: Id,
    date: String,
    data: Vec<Row>
}

impl Database {
    pub async fn init(id: Id) -> Self {
        println!("Initialising database for {id}");
        let projects = get_projects(id).await;

        let database = Self { root_id: id, date: Utc::now().to_string(), data: projects };

        database.write_to_disk();

        database
    }

    pub async fn start_export(&self) {
        for row in self.data.clone() {
            let initial_status = row.status;
            let updated_row = row.request_export().await;
            
            let is_updated = initial_status != updated_row.status;
            if is_updated && initial_status.needs_request() {
                let sleep_time = Duration::seconds(10).to_std().unwrap();
                thread::sleep(sleep_time);
            }
        }
    }

    //pub fn find_row(&self) -> Row {}
    fn write_to_disk(&self) {
        let file = fs::File::create("database.json").unwrap();
        serde_json::to_writer_pretty(&file, self).unwrap();
    }
}


#[derive(Debug, Serialize, Deserialize, Clone)]
struct Row {
    id: Id,
    entity_type: Type,
    status: Status,
    parent_id: Option<Id>,
    path: String,
}

impl Row {
    const fn new(id: Id, entity_type: Type, parent_id: Option<Id>, status: Option<Status>, path: String) -> Self {
       Self {
            id,
            entity_type,
            status: match status {
                Some(s) => s,
                None => Status::None
            },
            parent_id,
            path,
        }
    }

    async fn get_status(&self) -> Status {
        let client = get_http_client();

        let url = format!("https://gitlab.com/api/v4/projects/{0}/export", self.id);
        let response = client
            .get(&url)
            .send()
            .await
            .unwrap()
            .json::<StatusResponse>()
            .await
            .unwrap();
        
        Status::from_str(&response.export_status)
    }

    fn update_status(&self, status: Status) -> Self {
        Self {
            id: self.id,
            entity_type: self.entity_type,
            status,
            parent_id: self.parent_id,
            path: self.path.clone()
        }
    }

    async fn request_export(&self) -> Row {
        if self.entity_type != Type::Project {
            println!("Item is not a project");
            return self.clone();
        }

        let id = self.id;

        let url = match self.entity_type {
            Type::Group => format!("https://gitlab.com/api/v4/groups/{id}/export"),
            Type::Project => format!("https://gitlab.com/api/v4/projects/{id}/export"),
        };

        let status = self.get_status().await;

        if !status.needs_request() {
            return self.update_status(status);
        }

        let client = get_http_client();
        let response = client
            .post(&url)
            .send()
            .await
            .unwrap()
            .json::<ExportResponse>()
            .await
            .unwrap();

        response.error.map_or_else(|| self.update_status(Status::Queued), |err| {
            println!("{err:?}");
            self.clone()
        })
    }
}

#[derive(Debug, PartialEq, Clone, Copy, Serialize, Deserialize)]
enum Type {
    Project, Group
}

#[derive(Debug, PartialEq, Clone, Copy, Serialize, Deserialize)]
enum Status {
    None,
    Queued,
    Started,
    Finished,
    RegenerationInProgress,
    Unknown,
}

impl Status {
    fn from_str(status: &str) -> Self {
        match status {
           "none" => Self::None,
           "queued" => Self::Queued,
           "started" => Self::Started,
           "finished" => Self::Finished,
           "regeneration_in_progress" => Self::RegenerationInProgress,
           _ => Self::Unknown, 
        }
    }

    fn needs_request(&self) -> bool {
        self == &Self::None || self == &Self::Unknown
    }
}

#[derive(Deserialize, Debug)]
struct StatusResponse {
    export_status: String
}

#[derive(Deserialize, Debug)]
struct ExportResponse {
    error: Option<String>,
}

#[derive(Deserialize, Debug)]
struct ProjectResponse {
    id: Id,
    path_with_namespace: String,
}

// @todo needs better architecture for this function
async fn get_projects(root_id: Id) -> Vec<Row> {
    let client = get_http_client();
    let mut projects: Vec<ProjectResponse> = vec![];
    let mut current_page = 1;

    loop {
        let page = current_page.to_string();
        let params = &[
            ("page", page.as_str()),
            ("per_page", "100"),
            ("include_subgroups", "true")
        ];
        let url = Url::parse_with_params(&format!("https://gitlab.com/api/v4/groups/{root_id}/projects"), params).unwrap();
        let mut res = client
            .get(url.as_str())
            .send()
            .await
            .unwrap()
            .json::<Vec<ProjectResponse>>()
            .await
            .unwrap();

        if res.is_empty() {
            break;
        }
        
        current_page += 1;
        projects.append(&mut res);
    }

    let mut updated_projects: Vec<Row> = vec![];
    for project in projects {
        let row = Row::new(project.id, Type::Project, None, None, project.path_with_namespace);
        let status = row.get_status().await;
        updated_projects.push(row.update_status(status));
    }

    updated_projects
}
