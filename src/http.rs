use reqwest::{Client, header};
use std::env;

const PRIVATE_TOKEN_VARIABLE: &str = "GITLAB_TOKEN";

pub fn get_http_client() -> Client {
    let token = get_private_token();
    let mut headers = header::HeaderMap::new();
    headers.insert("PRIVATE-TOKEN", header::HeaderValue::from_str(&token).expect("Invalid token"));


    reqwest::Client::builder()
        .default_headers(headers)
        .build()
        .expect("Unable to build http client")
}

fn get_private_token() -> String {
    env::var(PRIVATE_TOKEN_VARIABLE)
        .unwrap_or_else(|_| panic!("{PRIVATE_TOKEN_VARIABLE} variable must be set"))
}