use clap::{Parser, Subcommand, Args};
use crate::database::{Database, Id};

#[derive(Debug, Parser)]
#[command(about = "A CLI tool to manage Gitlab backups", long_about = None)]
pub struct Cli {
    #[command(subcommand)]
    command: Commands,
}

impl Cli {
    pub async fn run() {
        let args = Self::parse();
        match args.command {
            Commands::Export(export) => {
                export.handle().await;
            }
            Commands::Import(import) => {
                println!("{:?}", &import);
            }
        }
    }
}

#[derive(Debug, Subcommand)]
enum Commands {
    #[command(about = "Import backups into Gitlab instance", long_about = None)]
    Import(Import),

    #[command(about = "Export projects and/or groups from a root group", long_about= None)]
    Export(Export),
}

#[derive(Debug, Args)]
struct Import {}

#[derive(Debug, Args)]
struct Export {
    #[arg(short = 'g', long, required = true)]
    group_id: Id,

    #[arg(short = 'w', long, default_value_t = true)]
    write_to_disk: bool,

    // @todo
    //#[arg(short = 'p', long, default_value_t = "./database.json")]
    //database_path: &str,
    
    // @todo
    //#[arg(short = 'h', long, default_value_t = "gitlab.com")]
    //host: String
}

impl Export {
    async fn handle(&self) {
        let group_id = self.group_id;
        let database = Database::init(group_id).await;

        // @todo add a check to diff last created database file if needed
        database.start_export().await;
    }
}