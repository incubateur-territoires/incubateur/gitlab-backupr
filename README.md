# Gitlab backupr

CLI tool to manage exports and imports from Gitlab instances

## Env

| name | required |
| --- | --- |
| `GITLAB_TOKEN` | ✅ |

## Flow

__Main commands:__
```sh
A CLI tool to manage Gitlab backups

Usage: glab-backupr <COMMAND>

Commands:
  import  Import backups into Gitlab instance
  export  Export projects and/or groups from a root group
  help    Print this message or the help of the given subcommand(s)

Options:
  -h, --help  Print help
```

__Export command:__
```sh
Export projects and/or groups from a root group

Usage: glab-backupr export [OPTIONS] --group-id <GROUP_ID>

Options:
  -g, --group-id <GROUP_ID>  
  -w, --write-to-disk        
  -h, --help                 Print help
```