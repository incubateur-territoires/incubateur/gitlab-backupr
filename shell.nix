{ pkgs ? import <nixpkgs> {} }:
  pkgs.mkShell {
    name = "generate gitlab backups";
    nativeBuildInputs = with pkgs; [ rustup cargo clippy gcc pkg-config openssl ];
    RUST_SRC_PATH = "${pkgs.rust.packages.stable.rustPlatform.rustLibSrc}";
}
